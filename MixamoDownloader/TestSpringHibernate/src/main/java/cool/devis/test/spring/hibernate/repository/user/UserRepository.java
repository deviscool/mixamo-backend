/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.test.spring.hibernate.repository.user;

import cool.devis.test.spring.hibernate.entity.user.User;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;

/**
 *
 * @author eusebiu
 */
public interface UserRepository extends JpaRepository<User, Long>{
    
}
