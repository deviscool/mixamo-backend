/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.webservice.controller;

import cool.devis.mixamo.util.service.AnimationService;
import cool.devis.mixamo.util.service.CharacterService;
import cool.devis.mixamo.util.pojo.Pagination;
import cool.devis.mixamo.util.pojo.ProductsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Eusebiu
 */
@RestController()
@RequestMapping(path = "/character")
public class CharacterController {

    @Autowired
    CharacterService characterService;

    @RequestMapping(method = RequestMethod.GET)
    public ProductsResult getCharacters(@RequestParam(name = "page", defaultValue = "1", required = false) int page,
            @RequestParam(name = "limit", defaultValue = "20", required = false) int limit) {

        Pagination pagination = new Pagination();
        pagination.setPage(page);
        pagination.setLimit(limit);

        return characterService.getCharactersMixamoSite(pagination);

    }

    @RequestMapping(path = "sync", method = RequestMethod.POST)
    public void sync() {

        characterService.syncMixamoCharacters();
    }
}
