package cool.devis.mixamo.util.service;

import cool.devis.mixamo.util.pojo.PrimaryCharacter;
import cool.devis.mixamo.util.entity.Product;
import cool.devis.mixamo.util.repository.CharacterRepository;
import cool.devis.mixamo.util.pojo.Pagination;
import cool.devis.mixamo.util.pojo.ProductsResult;
import static cool.devis.mixamo.util.common.ProductCommon.getProductsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Eusebiu
 */
@Service
public class CharacterService {

    @Autowired
    ProductService productService;

    @Autowired
    CharacterRepository characterRepository;

    ProductService.ProductType productType = ProductService.ProductType.Character;

    @Value("${mixamo.api.key}")
    private String apiKey;

    public ProductsResult getCharactersMixamoSite(Pagination pagination) {
        return productService.getProducts(pagination, ProductService.ProductType.Character);
    }

    public PrimaryCharacter getPrimaryCharacter() {

        String url = "https://www.mixamo.com/api/v1/characters/primary";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("X-Api-Key", apiKey);
        headers.set("authorization", "Bearer eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MDg2MTc2NzExMTlfN2JkNzZjMzAtNjAyYi00YjM5LWJjOTktNTA2NmI0Y2U4M2U0X3VlMSIsImNsaWVudF9pZCI6Im1peGFtbzEiLCJ1c2VyX2lkIjoiNzc4RDJCNDk0QzhGNDNBQjBBMDRCODM4QEFkb2JlSUQiLCJzdGF0ZSI6IiIsInR5cGUiOiJhY2Nlc3NfdG9rZW4iLCJhcyI6Imltcy1uYTEiLCJmZyI6IlIzVUNXR0RXWFBQUU1BQUFBQUFBQUFBQVVRPT09PT09IiwicnRpZCI6IjE1MDg2MTc2NzExMjBfZDVlZjUyOTYtNDFkYy00MzAxLWI5NzQtZGM2YzE1M2E1YTM1X3VlMSIsIm9jIjoicmVuZ2EqbmExcioxNWY0MDllYzY4MSpGREpOSkVESjhONjE5RTI3M01RV0U5RUM4RyIsInJ0ZWEiOiIxNTA5ODI3MjcxMTIwIiwibW9pIjoiY2Q3ZDJiNzIiLCJjIjoiOHYyQ2NsOG0zWG56WGNjMGxZSFcvZz09IiwiZXhwaXJlc19pbiI6Ijg2NDAwMDAwIiwic2NvcGUiOiJvcGVuaWQsQWRvYmVJRCxmZXRjaF9zYW8sc2FvLmNyZWF0aXZlX2Nsb3VkIiwiY3JlYXRlZF9hdCI6IjE1MDg2MTc2NzExMTkifQ.SB2_QK0eXnZgoIN3GC9ctVRXsSOkbIjHB-3EDh7t15IO23BbLNw19rePZIMs1YG0926N2CVa2M41xCQ5AzKZFmFu3VAwkHHzMxZM_8DZTRCAfKjDM9y0OXtv7Bg9inft-era9fMtXofQkv0pHS_8q30_JFU9wrg8VlC7ern_-Ma4Aze9ZWOSOasT6CmtTrL__xJS0P6UdW_TLA1Y5hdjIlcuwHbCSj77aNrTUZzv9J3MbXMSN0ok8eXgETpiPaqpwpC4S-Uy9xEy-foyhqfsPqIA84uFs26vYVTbYhGxpl8yHdl73fChuB_PaWBK_VS4Cb7Gi2CjUABQgcYwjaQuxQ");

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<PrimaryCharacter> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, PrimaryCharacter.class);

        return exchange.getBody();
    }

    public void syncMixamoCharacters() {

        int pageNumber = 1;
        Pagination pagination = new Pagination();

        pagination.setLimit(10);
        pagination.setPage(pageNumber);

        ProductsResult characters = getCharactersMixamoSite(pagination);

        while (characters.getResults().size() > 0) {

            for (Product character : characters.getResults()) {
                if (characterRepository.findByUuid(character.getUuId()) == null) {
                    characterRepository.save(character);
                }
            }

            pagination.setPage(pageNumber++);

            characters = getCharactersMixamoSite(pagination);
        }

    }

    public Product findbyId(int id) {
        return characterRepository.findOne(id);
    }

    public Product findByUuid(String uuid) {
        return characterRepository.findByUuid(uuid);
    }

    public Product save(Product character) {
        return characterRepository.save(character);
    }

    public ProductsResult getCharacters(Pagination pagination) {

        PageRequest pageRequest = new PageRequest(pagination.getPage() - 1, pagination.getLimit());

        Page<Product> characters = characterRepository.findByType(productType.name(), pageRequest);

        return getProductsResult(characters);
    }
}
