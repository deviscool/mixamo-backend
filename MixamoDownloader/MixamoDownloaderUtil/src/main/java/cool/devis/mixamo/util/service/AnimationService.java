package cool.devis.mixamo.util.service;

import cool.devis.mixamo.util.entity.Product;
import cool.devis.mixamo.util.repository.AnimationRepository;
import cool.devis.mixamo.util.pojo.Pagination;
import cool.devis.mixamo.util.pojo.ProductsResult;
import static cool.devis.mixamo.util.common.ProductCommon.getProductsResult;
import cool.devis.mixamo.util.service.ProductService.ProductType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Eusebiu
 */
@Service
public class AnimationService {

    @Autowired
    ProductService productService;

    @Autowired
    AnimationRepository animationRepository;
    
    ProductType productType = ProductService.ProductType.Motion;

    public ProductsResult getAnimations(Pagination pagination) {

        PageRequest pageRequest = new PageRequest(pagination.getPage() - 1, pagination.getLimit());

        Page<Product> animations = animationRepository.findByType(productType.name(), pageRequest);

        return getProductsResult(animations);
    }

    public ProductsResult getAnimationsMixamoSite(Pagination pagination) {
        return productService.getProducts(pagination, productType);
    }

    public Product save(Product animation) {

        return animationRepository.save(animation);
    }

    public void syncMixamoAnimations() {

        int pageNumber = 1;
        Pagination pagination = new Pagination();

        pagination.setLimit(10);
        pagination.setPage(pageNumber);

        ProductsResult animations = getAnimationsMixamoSite(pagination);

        while (animations.getResults().size() > 0) {

            for (Product animation : animations.getResults()) {
                if (animationRepository.findByUuid(animation.getUuId()) == null) {
                    animationRepository.save(animation);
                }
            }

            pagination.setPage(pageNumber++);

            animations = getAnimationsMixamoSite(pagination);
        }

    }

    public Product findById(Integer id) {
        return animationRepository.findOne(id);
    }

}
