package cool.devis.test.spring.hibernate;

import ch.qos.logback.classic.Logger;
import cool.devis.test.spring.hibernate.shared.classes.Products.Product;
import cool.devis.test.spring.hibernate.repository.customer.ProductRepository;
import cool.devis.test.spring.hibernate.entity.customer.Customer;
import cool.devis.test.spring.hibernate.entity.user.User;
import cool.devis.test.spring.hibernate.repository.customer.CustomerRepository;
import cool.devis.test.spring.hibernate.repository.user.UserRepository;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {
    /**
     * @param args the command line arguments
     */
    
    
    private static final Logger log = (Logger) LoggerFactory.getLogger(Main.class);
    
    
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        
        //articleService.getAllArticles();
                
    }
    
    //@Autowired
    //private UserRepository userRepository;
    
    @Autowired
    private ProductRepository productRepository;
    
    @Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
                    
                    
                    Product product=new Product();
                    
                    //product.setName("test");
                    
                    productRepository.save(product);
                    
//                    User user=new User();
//                    
//                    user.setId(1L);
//                    
//                    userRepository.save(user);
                    
                    if(1==1){
                        return;
                    }
			// save a couple of customers
			repository.save(new Customer("Jack", "Bauer"));
			repository.save(new Customer("Chloe", "O'Brian"));
			repository.save(new Customer("Kim", "Bauer"));
			repository.save(new Customer("David", "Palmer"));
			repository.save(new Customer("Michelle", "Dessler"));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Customer customer = repository.findOne(1L);
			log.info("Customer found with findOne(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			for (Customer bauer : repository.findByLastName("Bauer")) {
				log.info(bauer.toString());
			}
			log.info("");
		};
	}
}
