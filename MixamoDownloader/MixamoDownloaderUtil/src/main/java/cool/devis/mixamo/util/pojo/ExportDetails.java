/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.pojo;

/**
 *
 * @author Eusebiu
 */

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"type",
"description",
"category",
"character_type",
"name",
"motion_id",
"details",
"source"
})
public class ExportDetails {

@JsonProperty("id")
private String id;
@JsonProperty("type")
private String type;
@JsonProperty("description")
private String description;
@JsonProperty("category")
private String category;
@JsonProperty("character_type")
private String characterType;
@JsonProperty("name")
private String name;
@JsonProperty("motion_id")
private String motionId;
@JsonProperty("details")
private AnimationDetails details;
@JsonProperty("source")
private String source;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("type")
public String getType() {
return type;
}

@JsonProperty("type")
public void setType(String type) {
this.type = type;
}

@JsonProperty("description")
public String getDescription() {
return description;
}

@JsonProperty("description")
public void setDescription(String description) {
this.description = description;
}

@JsonProperty("category")
public String getCategory() {
return category;
}

@JsonProperty("category")
public void setCategory(String category) {
this.category = category;
}

@JsonProperty("character_type")
public String getCharacterType() {
return characterType;
}

@JsonProperty("character_type")
public void setCharacterType(String characterType) {
this.characterType = characterType;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("motion_id")
public String getMotionId() {
return motionId;
}

@JsonProperty("motion_id")
public void setMotionId(String motionId) {
this.motionId = motionId;
}

@JsonProperty("details")
public AnimationDetails getDetails() {
return details;
}

@JsonProperty("details")
public void setDetails(AnimationDetails details) {
this.details = details;
}

@JsonProperty("source")
public String getSource() {
return source;
}

@JsonProperty("source")
public void setSource(String source) {
this.source = source;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
