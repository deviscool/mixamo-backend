/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.pojo;

/**
 *
 * @author eusebiu
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"limit",
"page",
"num_pages",
"num_results"
})
public class Pagination {

@JsonProperty("limit")
private Integer limit;
@JsonProperty("page")
private Integer page;
@JsonProperty("num_pages")
private Integer numPages;
@JsonProperty("num_results")
private Integer numResults;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("limit")
public Integer getLimit() {
return limit;
}

@JsonProperty("limit")
public void setLimit(Integer limit) {
this.limit = limit;
}

@JsonProperty("page")
public Integer getPage() {
return page;
}

@JsonProperty("page")
public void setPage(Integer page) {
this.page = page;
}

@JsonProperty("num_pages")
public Integer getNumPages() {
return numPages;
}

@JsonProperty("num_pages")
public void setNumPages(Integer numPages) {
this.numPages = numPages;
}

@JsonProperty("num_results")
public Integer getNumResults() {
return numResults;
}

@JsonProperty("num_results")
public void setNumResults(Integer numResults) {
this.numResults = numResults;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
