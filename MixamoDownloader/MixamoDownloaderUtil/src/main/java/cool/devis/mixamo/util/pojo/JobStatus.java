package cool.devis.mixamo.util.pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"status",
"message",
"uuid",
"type",
"job_uuid",
"job_type",
"job_result"
})
public class JobStatus {

@JsonProperty("status")
private String status;
@JsonProperty("message")
private String message;
@JsonProperty("uuid")
private String uuid;
@JsonProperty("type")
private String type;
@JsonProperty("job_uuid")
private String jobUuid;
@JsonProperty("job_type")
private String jobType;
@JsonProperty("job_result")
private String jobResult;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("status")
public String getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}

@JsonProperty("message")
public String getMessage() {
return message;
}

@JsonProperty("message")
public void setMessage(String message) {
this.message = message;
}

@JsonProperty("uuid")
public String getUuid() {
return uuid;
}

@JsonProperty("uuid")
public void setUuid(String uuid) {
this.uuid = uuid;
}

@JsonProperty("type")
public String getType() {
return type;
}

@JsonProperty("type")
public void setType(String type) {
this.type = type;
}

@JsonProperty("job_uuid")
public String getJobUuid() {
return jobUuid;
}

@JsonProperty("job_uuid")
public void setJobUuid(String jobUuid) {
this.jobUuid = jobUuid;
}

@JsonProperty("job_type")
public String getJobType() {
return jobType;
}

@JsonProperty("job_type")
public void setJobType(String jobType) {
this.jobType = jobType;
}

@JsonProperty("job_result")
public String getJobResult() {
return jobResult;
}

@JsonProperty("job_result")
public void setJobResult(String jobResult) {
this.jobResult = jobResult;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}