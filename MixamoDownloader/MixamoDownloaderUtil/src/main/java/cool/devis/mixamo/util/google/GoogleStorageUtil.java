package cool.devis.mixamo.util.google;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author Eusebiu
 */
public final class GoogleStorageUtil {

    static final Logger LOG = (Logger) LoggerFactory.getLogger(GoogleStorageUtil.class);
    HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    JsonFactory JSON_FACTORY = new JacksonFactory();

    private GoogleCredential googleCredential = null;
    private Storage googleStorage = null;

    @Value("${google.storage.key}")
    String GOOGLE_STORAGE_KEY;
    @Value("${google.storage.email}")
    String GOOGLE_STORAGE_EMAIL;
    @Value("${google.storage.exponential.backoff.codes}")
    String EXPONENTIAL_BACKOFF_EXCEPTION_CODES;
    @Value("${google.storage.retry.count}")
    Integer GOOGLE_STORAGE_RETRY_COUNT;
    @Value("${google.exponential.backoff.miliseconds}")
    Integer MAXIMUM_BACKOFF_MILLISECONDS;
    @Value("${temp.folder}")
    String TEMP_FOLDER_PATH;

    public GoogleStorageUtil() throws GeneralSecurityException, IOException {

        this.googleCredential = GetGoogleStorageCredentials();
        this.googleStorage = getGoogleStorage();
    }

    public GoogleCredential GetGoogleStorageCredentials() throws GeneralSecurityException, IOException {

        File privateKey = new File(GOOGLE_STORAGE_KEY);

        GoogleCredential credential = new GoogleCredential.Builder().setTransport(HTTP_TRANSPORT)
                .setJsonFactory(JSON_FACTORY)
                .setServiceAccountId(GOOGLE_STORAGE_EMAIL)
                .setServiceAccountScopes(
                        Collections.singleton("https://www.googleapis.com/auth/devstorage.full_control"))
                .setServiceAccountPrivateKeyFromP12File(privateKey)
                .build();

        return credential;
    }

    public String GenerateGoogleStorageToken() throws GeneralSecurityException, IOException, Exception {
        GoogleCredential credentials = GetGoogleStorageCredentials();

        boolean operationExecuted = false;
        Integer retryCount = 0;

        while (!operationExecuted) {
            try {
                credentials.refreshToken();
                operationExecuted = true;

                return credentials.getAccessToken();

            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);

                LOG.error("Exception: ", ex);
            }
        }

        return "";
    }

    private Storage getGoogleStorage() throws GeneralSecurityException, IOException {
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        Storage storageService = new Storage.Builder(httpTransport, JSON_FACTORY, googleCredential).setApplicationName("test").build();

        return storageService;
    }

    public List<StorageObject> getStorageObjectListFromFolder(String bucketName, String folder) throws Exception {

        if (!folder.endsWith("/")) {
            folder = folder + "/";
        }

        return GetStorageObjectList(bucketName, folder);
    }

    public List<StorageObject> GetStorageObjectList(String bucketName, String pathPrefix) throws IOException, Exception {

        boolean operationExecuted = false;
        Integer retryCount = 0;

        while (!operationExecuted) {
            try {
                Storage.Objects.List listRequest = googleStorage.objects().list(bucketName);
                listRequest.setPrefix(pathPrefix);

                List<StorageObject> results = new ArrayList<>();
                Objects objects;

                // Iterate through each page of results, and add them to our results list.
                do {
                    objects = listRequest.execute();

                    List<StorageObject> items = objects.getItems();
                    if (items != null) {
                        // Add the items in this page of results to the list we'll return.
                        results.addAll(items);
                    } else {
                        return results;
                    }

                    // Get the next page, in the next iteration of this loop.
                    listRequest.setPageToken(objects.getNextPageToken());
                } while (null != objects.getNextPageToken());

                operationExecuted = true;

                return results;
            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);

                LOG.error("Exception: ", ex);
            }
        }

        return null;
    }

    public void downloadToOutputStream(String bucketName, String objectName,
            OutputStream data) throws IOException, InterruptedException, Exception {

        boolean operationExecuted = false;
        int retryCount = 0;

        while (!operationExecuted) {
            try {
                Storage.Objects.Get getObject = googleStorage.objects().get(bucketName, objectName);
                getObject.getMediaHttpDownloader().setDirectDownloadEnabled(false);
                getObject.executeMediaAndDownloadTo(data);

                operationExecuted = true;

                LOG.info("Download OK");
            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);

                LOG.error("Exception: ", ex);
            }
        }
    }

    public void downloadFile(String bucketName, String objectName, String downloadPath) throws IOException, Exception {
        boolean operationExecuted = false;
        Integer retryCount = 0;
        FileOutputStream fileOut = null;

        while (!operationExecuted) {
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                downloadToOutputStream(bucketName, objectName, out);

                fileOut = new FileOutputStream(new File(downloadPath));
                fileOut.write(out.toByteArray());

                operationExecuted = true;
                LOG.info("Downloaded " + out.toByteArray().length + " bytes");

            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);
                LOG.error("Exception: ", ex);

            } finally {
                if (fileOut != null) {

                    fileOut.close();
                }
            }
        }
    }

    public byte[] downloadFile(String storagePath) throws IOException, Exception {

        ByteArrayOutputStream out = null;

        try {

            String bucketName = storagePath.split("/")[0];
            String fileName = storagePath.replace(bucketName + "/", "");

            out = new ByteArrayOutputStream();

            downloadToOutputStream(bucketName, fileName, out);

            return out.toByteArray();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    public void moveFile(String sourceBucket, String sourceFile, String destinationBucket, String destinationFile) throws IOException, Exception {
        if (!sourceFile.equals(destinationFile)) {

            boolean operationExecuted = false;
            Integer retryCount = 0;

            while (!operationExecuted) {
                try {
                    Storage.Objects.Copy copy = googleStorage.objects().copy(sourceBucket, sourceFile, destinationBucket, destinationFile, null);
                    copy.execute();

                    Storage.Objects.Delete delete = googleStorage.objects().delete(sourceBucket, sourceFile);
                    delete.execute();

                    operationExecuted = true;

                } catch (IOException ex) {
                    retryCount = handleExponentialBackoff(ex, retryCount);

                    LOG.error("Exception: ", ex);
                }
            }

        }
    }

    public void moveFolder(String sourceBucket, String sourceFolder, String destinationBucket, String destinationFolder) throws IOException, Exception {

        if (!sourceFolder.endsWith("/")) {
            sourceFolder = sourceFolder + "/";
        }

        if (!destinationFolder.endsWith("/")) {
            destinationFolder = destinationFolder + "/";
        }

        List<StorageObject> sourceFiles = GetStorageObjectList(sourceBucket, sourceFolder);

        for (StorageObject sourceFile : sourceFiles) {

            moveFile(sourceBucket, sourceFile.getName(), destinationBucket, sourceFile.getName().replaceFirst(sourceFolder, destinationFolder));
        }
    }

    public void deleteFolder(String sourceBucket, String sourceFolder) throws IOException, Exception {

        if (!sourceFolder.endsWith("/")) {
            sourceFolder = sourceFolder + "/";
        }

        List<StorageObject> sourceFiles = GetStorageObjectList(sourceBucket, sourceFolder);

        for (StorageObject sourceFile : sourceFiles) {

            deleteFile(sourceBucket, sourceFile.getName());
        }
    }

    public void copyFolder(String sourceBucket, String sourceFolder, String destinationBucket, String destinationFolder) throws IOException, Exception {

        List<StorageObject> sourceFiles = GetStorageObjectList(sourceBucket, sourceFolder + "/");

        for (StorageObject sourceFile : sourceFiles) {

            copyFile(sourceBucket, sourceFile.getName(), destinationBucket, sourceFile.getName().replaceFirst(sourceFolder, destinationFolder));
        }
    }

    public void copyFolder(String sourceBucket, String sourceFolder, String destinationBucket, String destinationFolder, HashMap extendedActions) throws IOException, Exception {

        List<StorageObject> sourceFiles = GetStorageObjectList(sourceBucket, sourceFolder + "/");

        for (StorageObject sourceFile : sourceFiles) {

            if (extendedActions != null) {
                if (extendedActions.get("maximumLevel") != null) {

                    int maximumLevel = (int) extendedActions.get("maximumLevel");

                    if (sourceFile.getName().split("/").length > maximumLevel) {
                        continue;
                    }
                }
            }

            copyFile(sourceBucket, sourceFile.getName(), destinationBucket, sourceFile.getName().replaceFirst(sourceFolder, destinationFolder));
        }
    }

    public boolean checkIfFileExists(String bucket, String file) throws IOException, Exception {

        List<StorageObject> GetStorageObjectList = GetStorageObjectList(bucket, file);

        return GetStorageObjectList.stream().anyMatch((storageObject) -> (storageObject.getName().equals(file)));
    }

    public StorageObject getFile(String bucket, String filePath) throws IOException, Exception {

        List<StorageObject> storageObjectList = GetStorageObjectList(bucket, filePath);

        for (StorageObject storageObject : storageObjectList) {
            if (storageObject.getName().equals(filePath)) {
                return storageObject;
            }
        }
        return null;
    }

    public boolean checkIfFolderExists(String bucket, String folderName) throws IOException, Exception {

        List<StorageObject> GetStorageObjectList = GetStorageObjectList(bucket, folderName);

        return (GetStorageObjectList.size() > 0);
    }

    public StorageObject copyFile(String sourceBucket, String sourceFile, String destinationBucket, String destinationFile) throws IOException, Exception {

        Storage.Objects.Copy copy = googleStorage.objects().copy(sourceBucket, sourceFile, destinationBucket, destinationFile, null);
        StorageObject copiedObject = null;

        boolean operationExecuted = false;
        Integer retryCount = 0;

        while (!operationExecuted) {
            try {
                copiedObject = copy.execute();

                operationExecuted = true;

            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);

                LOG.error("Exception: ", ex);
            }
        }

        String sourceFileName = FilenameUtils.getName(sourceFile);
        String sourceParentFolder = FilenameUtils.getPath(sourceFile);
        String destinationFileName = FilenameUtils.getName(destinationFile);
        String destinationParentFolder = FilenameUtils.getPath(destinationFile);

        return copiedObject;
    }

    public void deleteFile(String storageFilePath) throws IOException, Exception {

        String bucketName = storageFilePath.split("/")[0];
        String fileName = storageFilePath.substring(storageFilePath.indexOf("/") + 1);

        deleteFile(bucketName, fileName);
    }

    public void deleteFile(String bucketName, String fileName) throws IOException, Exception {
        boolean operationExecuted = false;
        Integer retryCount = 0;

        while (!operationExecuted) {
            try {
                googleStorage.objects().delete(bucketName, fileName).execute();
                operationExecuted = true;

            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);

                LOG.error("Exception: ", ex);
            }
        }

    }

    public String getFileHash(String bucketName, String filePath) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        downloadToOutputStream(bucketName, filePath, out);
        ByteArrayInputStream is = new ByteArrayInputStream(out.toByteArray());

        byte[] buffer = new byte[100 * 1024];
        int read;
        String output = "";
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] sum = digest.digest();
            //metodo 1
            StringBuilder sb = new StringBuilder();
            //metodo 2
            for (int i = 0; i < sum.length; i++) {
                String hex = Integer.toHexString(0xff & sum[i]);
                if (hex.length() == 1) {
                    sb.append('0');
                }
                sb.append(hex);
            }
            output = sb.toString();
        } catch (Exception ex) {
            throw ex;
        } finally {
            try {
                is.close();
            } catch (Exception ex) {
                throw ex;
            }
        }
        return output;

    }

    public void uploadFile(String filePath, String bucketName, String uploadPath) throws FileNotFoundException, IOException, GeneralSecurityException, Exception {

        FileInputStream inputFile = new FileInputStream(new File(filePath));

        String mimeType = getFileMIMETypeByExtension(uploadPath, false);

        uploadStream(uploadPath, mimeType, inputFile, bucketName);

    }

    public void uploadFile(byte[] fileByteArray, String bucketName, String uploadPath) throws FileNotFoundException, IOException, GeneralSecurityException, Exception {

        ByteArrayInputStream inputFile = new ByteArrayInputStream(fileByteArray);

        String mimeType = "application/octet-stream";

        if (uploadPath.endsWith(".png")) {
            mimeType = "image/png";
        }

        uploadStream(uploadPath, mimeType, inputFile, bucketName);

    }

    public void uploadFileWithEncryptionKey(String filePath, String bucketName, String uploadPath, String base64CseKey, String base64CseKeyHash) throws FileNotFoundException, IOException, GeneralSecurityException, Exception {

        FileInputStream inputFile = new FileInputStream(new File(filePath));

        String mimeType = getFileMIMETypeByExtension(uploadPath, false);

        if (uploadPath.endsWith(".png")) {
            mimeType = "image/png";
        }

        uploadStream(uploadPath, mimeType, inputFile, bucketName, base64CseKey, base64CseKeyHash);

    }

    /**
     * Uploads data to an object in a bucket.
     *
     * @param name the name of the destination object.
     * @param contentType the MIME type of the data.
     * @param stream the data - for instance, you can use a FileInputStream to
     * upload a file.
     * @param bucketName the name of the bucket to create the object in.
     * @throws java.io.IOException
     * @throws java.security.GeneralSecurityException
     */
    private void uploadStream(String name, String contentType, InputStream stream, String bucketName) throws IOException, Exception {

        boolean operationExecuted = false;
        int retryCount = 0;

        while (!operationExecuted) {
            try {
                InputStreamContent contentStream = new InputStreamContent(contentType, stream);
                StorageObject objectMetadata = new StorageObject().setName(name);

                // Set the destination object name                        
                // Set the access control list to publicly read-only
                //.setAcl(Arrays.asList(
                //  new ObjectAccessControll().setEntity("allUsers").setRole("READER")));
                // Do the insert
                Storage.Objects.Insert insertRequest = googleStorage.objects().insert(
                        bucketName, objectMetadata, contentStream);

                insertRequest.execute();

                operationExecuted = true;
                LOG.info("upload OK");

            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);

                LOG.error("Exception: ", ex);
            }
        }

    }

    public void uploadStream(String name, String contentType, InputStream stream, String bucketName, String base64CseKey, String base64CseKeyHash) throws IOException, Exception {

        boolean operationExecuted = false;
        int retryCount = 0;

        while (!operationExecuted) {
            try {
                InputStreamContent contentStream = new InputStreamContent(contentType, stream);
                StorageObject objectMetadata = new StorageObject().setName(name);

                // Set the destination object name                        
                // Set the access control list to publicly read-only
                //.setAcl(Arrays.asList(
                //  new ObjectAccessControll().setEntity("allUsers").setRole("READER")));
                // Do the insert
                Storage.Objects.Insert insertRequest = googleStorage.objects().insert(
                        bucketName, objectMetadata, contentStream);

                insertRequest.getMediaHttpUploader().setDisableGZipContent(true);

                final HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.set("x-goog-encryption-algorithm", "AES256");
                httpHeaders.set("x-goog-encryption-key", base64CseKey);
                httpHeaders.set("x-goog-encryption-key-sha256", base64CseKeyHash);

                insertRequest.setRequestHeaders(httpHeaders);

                insertRequest.execute();

                operationExecuted = true;

            } catch (GoogleJsonResponseException ex) {
                LOG.info("Error uploading: " + ex.getContent());
                LOG.error("Error uploading" + ex.getContent());

            } catch (IOException ex) {
                retryCount = handleExponentialBackoff(ex, retryCount);

                LOG.error("Exception: ", ex);
            }
        }
    }

    public void zipFolder(String bucketName, String folderPath, String destinationBucketName, String zipPath) throws IOException, FileNotFoundException, GeneralSecurityException, Exception {
        zipFolder(bucketName, folderPath, destinationBucketName, zipPath, null);
    }

    public void zipFolder(File dir, File zipFile) throws IOException {
        FileOutputStream fout = new FileOutputStream(zipFile);
        ZipOutputStream zout = new ZipOutputStream(fout);
        zipSubFolder("", dir, zout);
        zout.close();
    }

    private void zipSubFolder(String basePath, File dir, ZipOutputStream zout) throws IOException {
        byte[] buffer = new byte[4096];
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                String path = basePath + file.getName() + "/";
                // zout.putNextEntry(new ZipEntry(path));
                zipSubFolder(path, file, zout);
                zout.closeEntry();
            } else {
                FileInputStream fin = new FileInputStream(file);
                zout.putNextEntry(new ZipEntry(basePath + file.getName()));
                int length;
                while ((length = fin.read(buffer)) > 0) {
                    zout.write(buffer, 0, length);
                }
                zout.closeEntry();
                fin.close();
            }
        }
    }

    public void zipFolder(String bucketName, String folderPath, String destinationBucketName, String zipPath, HashMap extendedActions) throws FileNotFoundException, IOException, GeneralSecurityException, Exception {

        if (folderPath.endsWith("/")) {
            folderPath = folderPath.substring(0, folderPath.length() - 1);
        }

        List<StorageObject> items = getStorageObjectListFromFolder(bucketName, folderPath);

        String tempFolderPath = TEMP_FOLDER_PATH;

        String tempFile = tempFolderPath + "tmpFolder" + UUID.randomUUID().toString() + ".zip";

        OutputStream out = new FileOutputStream(new File(tempFile));
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        ZipOutputStream testZip = new ZipOutputStream(out);

        String baseName = FilenameUtils.getBaseName(folderPath);

        LOG.info("Base name: " + baseName);
        for (StorageObject storageObject : items) {

            if (extendedActions != null) {
                if (extendedActions.get("maximumLevel") != null) {

                    int maximumLevel = (int) extendedActions.get("maximumLevel");

                    if (storageObject.getName().split("/").length > maximumLevel) {
                        continue;
                    }
                }
            }

            ZipEntry zipEntry = new ZipEntry(baseName + storageObject.getName().substring(folderPath.length()));  ////????

            downloadToOutputStream(bucketName, storageObject.getName(), outByteStream);

            testZip.putNextEntry(zipEntry);

            testZip.write(outByteStream.toByteArray());
            outByteStream.flush();
            outByteStream.reset();
            testZip.closeEntry();

        }

        outByteStream.close();
        testZip.close();

        uploadFile(tempFile, destinationBucketName, zipPath);

        new File(tempFile).delete();
    }

    public void zipFiles(String bucketName, ArrayList<String> filePaths, String destinationBucketName, String zipPath, String baseName) throws FileNotFoundException, IOException, GeneralSecurityException, Exception {

        String tempFolderPath = TEMP_FOLDER_PATH;

        String tempFile = tempFolderPath + "temp" + UUID.randomUUID().toString() + ".zip";

        OutputStream out = new FileOutputStream(new File(tempFile));
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        ZipOutputStream testZip = new ZipOutputStream(out);

        //FilenameUtils.getBaseName(folderPath);
        LOG.info("Base name: " + baseName);
        for (String filePath : filePaths) {
            ZipEntry zipEntry = null;
            if (filePath.contains("AIP")) {
                zipEntry = new ZipEntry(baseName + filePath.substring(filePath.indexOf("AIP")));
            } else {
                zipEntry = new ZipEntry(baseName + FilenameUtils.getName(filePath));
            }

            downloadToOutputStream(bucketName, filePath, outByteStream);

            testZip.putNextEntry(zipEntry);

            testZip.write(outByteStream.toByteArray());
            outByteStream.flush();
            outByteStream.reset();
            testZip.closeEntry();

        }

        outByteStream.close();
        testZip.close();

        uploadFile(tempFile, destinationBucketName, zipPath);

        new File(tempFile).delete();
    }

    public void zipFolders(String bucketName, ArrayList<String> folderPaths, String destinationBucketName, String zipPath, HashMap extendedActions) throws FileNotFoundException, IOException, GeneralSecurityException, Exception {

        String tempFolderPath = TEMP_FOLDER_PATH;

        String tempFile = tempFolderPath + "tmpFolder" + UUID.randomUUID().toString() + ".zip";

        OutputStream out = new FileOutputStream(new File(tempFile));
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        ZipOutputStream testZip = new ZipOutputStream(out);

        for (String folderPath : folderPaths) {
            if (folderPath.endsWith("/")) {
                folderPath = folderPath.substring(0, folderPath.length() - 1);
            }

            List<StorageObject> items = GetStorageObjectList(bucketName, folderPath);

            String baseName = FilenameUtils.getBaseName(folderPath);

            baseName = FilenameUtils.getBaseName(folderPath.substring(0, folderPath.lastIndexOf(baseName) - 1)) + "/" + baseName;

            if (extendedActions != null) {
                if (extendedActions.get("replaceFirstInBaseName") != null) {

                    HashMap replaceFirst = (HashMap) extendedActions.get("replaceFirstInBaseName");

                    String toReplace = (String) replaceFirst.get("toReplace");
                    String replaceWith = (String) replaceFirst.get("replaceWith");
                    baseName = baseName.replaceFirst(toReplace, replaceWith);
                }
            }

            LOG.info("Base name: " + baseName);
            for (StorageObject storageObject : items) {

                if (extendedActions != null) {
                    if (extendedActions.get("maximumLevel") != null) {

                        int maximumLevel = (int) extendedActions.get("maximumLevel");

                        if (storageObject.getName().split("/").length > maximumLevel) {
                            continue;
                        }
                    }
                }
                ZipEntry zipEntry = new ZipEntry(baseName + storageObject.getName().substring(folderPath.length()));

                downloadToOutputStream(bucketName, storageObject.getName(), outByteStream);

                testZip.putNextEntry(zipEntry);

                testZip.write(outByteStream.toByteArray());
                outByteStream.flush();
                outByteStream.reset();
                testZip.closeEntry();

            }
        }
        outByteStream.close();
        testZip.close();

        uploadFile(tempFile, destinationBucketName, zipPath);

        new File(tempFile).delete();
    }

    public byte[] getStorageFileByteArray(String bucketName, String fileName) throws IOException, GeneralSecurityException, Exception {

        byte[] byteArray = null;

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            downloadToOutputStream(bucketName, fileName, out);
            byteArray = out.toByteArray();
        } finally {
            out.close();
        }

        return byteArray;
    }

    public String getFileMIMETypeByExtension(String fileName) throws Exception {

        return getFileMIMETypeByExtension(fileName, true);
    }

    public String getFileMIMETypeByExtension(String fileName, boolean throwExceptionIfNotRecognized) throws Exception {

        String MIMEType = "";

        String extension = FilenameUtils.getExtension(fileName);

        switch (extension.toLowerCase()) {
            case "pdf":
                MIMEType = "application/pdf";
                break;
            case "txt":
                MIMEType = "text/plain";
                break;
            case "p7m":
                MIMEType = "application/pkcs7-mime";
                break;
            case "tsd":
                MIMEType = "application/timestamped-data";
                break;
            case "xml":
                MIMEType = "text/xml";
                break;
            case "zip":
                MIMEType = "application/zip";
                break;
            case "eml":
                MIMEType = "message/rfc822";
                break;
            case "tiff":
            case "tif":
                MIMEType = "image/tiff";
                break;
            case "jpeg":
            case "jpg":
            case "jpe":
            case "jfif":
            case "jfif-tbnl":
                MIMEType = "image/jpeg";
                break;
            case "doc":
                MIMEType = "application/msword";
                break;
            case "docx":
                MIMEType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                break;
            case "xls":
                MIMEType = "application/excel";
                break;
            case "xlsx":
                MIMEType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                break;
            case "ppt":
                MIMEType = "application/mspowerpoint";
                break;
            case "pptx":
                MIMEType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                break;
            case "ods":
                MIMEType = "application/vnd.oasis.opendocument.spreadsheet";
                break;
            case "odp":
                MIMEType = "application/vnd.oasis.opendocument.presentation";
                break;
            case "odg":
                MIMEType = "application/vnd.oasis.opendocument.graphics";
                break;
            case "odb":
                MIMEType = "application/vnd.oasis.opendocument.database";
                break;
            case "png":
                MIMEType = "image/png";
                break;
        }

        if (MIMEType.isEmpty()) {

            if (throwExceptionIfNotRecognized) {
                throw new Exception("MIME Type not recognized");
            } else {
                MIMEType = "application/octet-stream";
            }
        }
        return MIMEType;
    }

    public int getStatusCodeFromHttpResponseException(Exception ex) {
        int statusCode = 0;

        if (ex instanceof HttpResponseException) {

            HttpResponseException httpEx = (HttpResponseException) ex;

            statusCode = httpEx.getStatusCode();

            LOG.info("Status Code: " + statusCode);
        }

        return statusCode;
    }

    public int getStatusCodeFromExceptionUsingPattern(Exception ex) {
        int statusCode = 0;

        Matcher matcher = Pattern.compile("^Server returned HTTP response code: (\\d+)").matcher(ex.getMessage());

        if (matcher.find()) {
            statusCode = Integer.parseInt(matcher.group(1));
        }

        return statusCode;
    }

    public int getStatusCode(Exception ex) {
        int statusCode = getStatusCodeFromHttpResponseException(ex);

        if (statusCode == 0) {
            statusCode = getStatusCodeFromExceptionUsingPattern(ex);
        }

        return statusCode;
    }

    public boolean checkIfExceptionNeedsToBeHandledByExponentialBackoff(int statusCode) {
        boolean useExponentialBackoff = false;

        String exceptionCodes = EXPONENTIAL_BACKOFF_EXCEPTION_CODES;

        if (exceptionCodes != null && !exceptionCodes.equals("")) {
            String[] exceptionCodesList = exceptionCodes.split(",");

            for (String exceptionCode : exceptionCodesList) {
                if (Integer.parseInt(exceptionCode.trim()) == statusCode) {
                    useExponentialBackoff = true;
                    break;
                }
            }
        }

        return useExponentialBackoff;
    }

    public int handleExponentialBackoff(Exception ex, int retryCount) throws Exception {
        int statusCode = this.getStatusCode(ex);

        if (statusCode != 0) {
            boolean useExponentialBackoff = checkIfExceptionNeedsToBeHandledByExponentialBackoff(statusCode);

            if (useExponentialBackoff) {
                if (++retryCount > GOOGLE_STORAGE_RETRY_COUNT) {
                    throw ex;
                }

                long sleepTime = (long) Math.min(Math.pow(2, retryCount) * 1000 + Math.floor(Math.random() * 1000) + 1, MAXIMUM_BACKOFF_MILLISECONDS);
                Thread.sleep(sleepTime);

                return retryCount;
            }
            throw ex;
        }
        throw ex;
    }

    public StorageObject patch(String bucket, String filePath, StorageObject patch) throws IOException, IOException {

        Storage.Objects.Patch patch1 = googleStorage.objects().patch(bucket, filePath, patch);

        return patch1.execute();
    }
}
