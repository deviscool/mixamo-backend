/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.pojo;

/**
 *
 * @author Eusebiu
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"format",
"skin",
"fps",
"reducekf"
})
public class ExportPreferences {

@JsonProperty("format")
private String format;
@JsonProperty("skin")
private String skin;
@JsonProperty("fps")
private String fps;
@JsonProperty("reducekf")
private String reducekf;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("format")
public String getFormat() {
return format;
}

@JsonProperty("format")
public void setFormat(String format) {
this.format = format;
}

@JsonProperty("skin")
public String getSkin() {
return skin;
}

@JsonProperty("skin")
public void setSkin(String skin) {
this.skin = skin;
}

@JsonProperty("fps")
public String getFps() {
return fps;
}

@JsonProperty("fps")
public void setFps(String fps) {
this.fps = fps;
}

@JsonProperty("reducekf")
public String getReducekf() {
return reducekf;
}

@JsonProperty("reducekf")
public void setReducekf(String reducekf) {
this.reducekf = reducekf;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
