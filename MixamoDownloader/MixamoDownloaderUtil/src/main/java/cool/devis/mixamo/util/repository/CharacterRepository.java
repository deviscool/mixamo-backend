/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.repository;

import cool.devis.mixamo.util.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author eusebiu
 */
public interface CharacterRepository extends JpaRepository<Product, Integer> {

    public Product findByUuid(String uuid);

    public Page<Product> findByType(String uuid, Pageable pageable);
}
