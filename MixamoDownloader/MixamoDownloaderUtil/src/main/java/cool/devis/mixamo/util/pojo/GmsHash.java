/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.pojo;

/**
 *
 * @author Eusebiu
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"model-id",
"mirror",
"trim",
"inplace",
"arm-space",
"params"
})
public class GmsHash {

@JsonProperty("model-id")
private Integer modelId;
@JsonProperty("mirror")
private Boolean mirror;
@JsonProperty("trim")
private List<Double> trim = null;
@JsonProperty("inplace")
private Boolean inplace;
@JsonProperty("arm-space")
private Integer armSpace;
@JsonProperty("params")
private List<List<String>> params = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("model-id")
public Integer getModelId() {
return modelId;
}

@JsonProperty("model-id")
public void setModelId(Integer modelId) {
this.modelId = modelId;
}

@JsonProperty("mirror")
public Boolean getMirror() {
return mirror;
}

@JsonProperty("mirror")
public void setMirror(Boolean mirror) {
this.mirror = mirror;
}

@JsonProperty("trim")
public List<Double> getTrim() {
return trim;
}

@JsonProperty("trim")
public void setTrim(List<Double> trim) {
this.trim = trim;
}

@JsonProperty("inplace")
public Boolean getInplace() {
return inplace;
}

@JsonProperty("inplace")
public void setInplace(Boolean inplace) {
this.inplace = inplace;
}

@JsonProperty("arm-space")
public Integer getArmSpace() {
return armSpace;
}

@JsonProperty("arm-space")
public void setArmSpace(Integer armSpace) {
this.armSpace = armSpace;
}

@JsonProperty("params")
public List<List<String>> getParams() {
return params;
}

@JsonProperty("params")
public void setParams(List<List<String>> params) {
this.params = params;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
