/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.repository;

import cool.devis.mixamo.util.entity.User;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author eusebiu
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    public User findByEmail(String email);
    public User findByConfirmationToken(String token);
}
