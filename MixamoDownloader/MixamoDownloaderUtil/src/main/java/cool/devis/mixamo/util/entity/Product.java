package cool.devis.mixamo.util.entity;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 *
 * @author Eusebiu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "type",
    "description",
    "category",
    "character_type",
    "name",
    "thumbnail",
    "thumbnail_animated",
    "motion_id",
    "motions",
    "source"
})
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(unique = true)
    @JsonProperty("id")
    private String uuid;
    @JsonProperty("type")
    private String type;
    @JsonProperty("description")
    private String description;
    @JsonProperty("category")
    private String category;
    @JsonProperty("character_type")
    private String characterType;
    @JsonProperty("name")
    private String name;
    @JsonProperty("thumbnail")
    private String thumbnail;
    @JsonProperty("thumbnail_animated")
    private String thumbnailAnimated;
    @JsonProperty("motion_id")
    private String motionId;
    @JsonProperty("motions")
    @Transient
    private Object motions;
    @JsonProperty("source")
    private String source;
    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getUuId() {
        return uuid;
    }

    @JsonProperty("id")
    public void seUuId(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("character_type")
    public String getCharacterType() {
        return characterType;
    }

    @JsonProperty("character_type")
    public void setCharacterType(String characterType) {
        this.characterType = characterType;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("thumbnail")
    public String getThumbnail() {
        return thumbnail;
    }

    @JsonProperty("thumbnail")
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @JsonProperty("thumbnail_animated")
    public String getThumbnailAnimated() {
        return thumbnailAnimated;
    }

    @JsonProperty("thumbnail_animated")
    public void setThumbnailAnimated(String thumbnailAnimated) {
        this.thumbnailAnimated = thumbnailAnimated;
    }

    @JsonProperty("motion_id")
    public String getMotionId() {
        return motionId;
    }

    @JsonProperty("motion_id")
    public void setMotionId(String motionId) {
        this.motionId = motionId;
    }

    @JsonProperty("motions")
    public Object getMotions() {
        return motions;
    }

    @JsonProperty("motions")
    public void setMotions(Object motions) {
        this.motions = motions;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

}
