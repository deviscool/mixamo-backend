/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.test.spring.hibernate.repository.customer;

import cool.devis.test.spring.hibernate.shared.classes.Products.Product;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author eusebiu
 */
public interface ProductRepository extends CrudRepository<Product, Long>{
    
}
