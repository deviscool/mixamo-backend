/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cool.devis.mixamo.util.pojo.Pagination;
import cool.devis.mixamo.util.pojo.ProductsResult;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author eusebiu
 */
@Service
public class ProductService {

    @Value("${mixamo.api.key}")
    private String apiKey;

    public static enum ProductType {
        Motion, Character
    };

    public ProductsResult getProducts(Pagination pagination, ProductType productType) {

        if (pagination == null) {
            pagination = new Pagination();

            pagination.setPage(1);
            pagination.setLimit(20);
        }

        String url = "https://www.mixamo.com/api/v1/products?page=" + pagination.getPage() + "&limit=" + pagination.getLimit()
                + "&type=" + productType.name();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("X-Api-Key", apiKey);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<ProductsResult> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, ProductsResult.class);

        return exchange.getBody();

    }

    public String getAllProductsJsonString() {

        String url = "https://www.mixamo.com/api/v1/products?page=1&limit=24";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("X-Api-Key", apiKey);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        return exchange.getBody();

    }

    public JsonNode getAllProductsJson() throws IOException {

        String allProducts = getAllProductsJsonString();

        ObjectMapper mapper = new ObjectMapper();

        JsonNode jsonAllProducts = mapper.readTree(allProducts);

        return jsonAllProducts;

    }

}
