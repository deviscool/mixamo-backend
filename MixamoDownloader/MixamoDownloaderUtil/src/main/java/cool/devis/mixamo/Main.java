package cool.devis.mixamo;

import cool.devis.mixamo.util.service.AnimationService;
import cool.devis.mixamo.util.service.CharacterAnimationService;
import cool.devis.mixamo.util.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);

    }

    @Bean
    @Autowired
    public CommandLineRunner init(CharacterService characterService, AnimationService animationService, CharacterAnimationService charAnimationService) {

        return (args) -> {

        };
    }
}
