/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.pojo;

/**
 *
 * @author Eusebiu
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"gms_hash",
"preferences",
"character_id",
"type",
"product_name"
})
public class ExportRequest {

@JsonProperty("gms_hash")
private List<GmsHash> gmsHash = null;
@JsonProperty("preferences")
private ExportPreferences preferences;
@JsonProperty("character_id")
private String characterId;
@JsonProperty("type")
private String type;
@JsonProperty("product_name")
private String productName;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("gms_hash")
public List<GmsHash> getGmsHash() {
return gmsHash;
}

@JsonProperty("gms_hash")
public void setGmsHash(List<GmsHash> gmsHash) {
this.gmsHash = gmsHash;
}

@JsonProperty("preferences")
public ExportPreferences getPreferences() {
return preferences;
}

@JsonProperty("preferences")
public void setPreferences(ExportPreferences preferences) {
this.preferences = preferences;
}

@JsonProperty("character_id")
public String getCharacterId() {
return characterId;
}

@JsonProperty("character_id")
public void setCharacterId(String characterId) {
this.characterId = characterId;
}

@JsonProperty("type")
public String getType() {
return type;
}

@JsonProperty("type")
public void setType(String type) {
this.type = type;
}

@JsonProperty("product_name")
public String getProductName() {
return productName;
}

@JsonProperty("product_name")
public void setProductName(String productName) {
this.productName = productName;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}


