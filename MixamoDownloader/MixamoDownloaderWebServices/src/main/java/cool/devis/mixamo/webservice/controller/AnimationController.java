package cool.devis.mixamo.webservice.controller;

import cool.devis.mixamo.util.service.AnimationService;
import cool.devis.mixamo.util.pojo.Pagination;
import cool.devis.mixamo.util.pojo.ProductsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Eusebiu
 */
@RestController()
@RequestMapping(path = "animation")
public class AnimationController {

    @Autowired
    AnimationService animationService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ProductsResult getAnimations(@RequestParam(name = "page", defaultValue = "1", required = false) int page,
            @RequestParam(name = "limit", defaultValue = "20", required = false) int limit) {

        Pagination pagination = new Pagination();
        pagination.setPage(page);
        pagination.setLimit(limit);

        return animationService.getAnimations(pagination);

    }

    @RequestMapping(path = "sync", method = RequestMethod.POST)
    public void sync() {

        animationService.syncMixamoAnimations();
    }

}
