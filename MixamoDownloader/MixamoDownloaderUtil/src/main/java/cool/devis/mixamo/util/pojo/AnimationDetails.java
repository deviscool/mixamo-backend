/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.pojo;

/**
 *
 * @author Eusebiu
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"supports_inplace",
"loopable",
"default_frame_length",
"duration",
"gms_hash"
})
public class AnimationDetails {

@JsonProperty("supports_inplace")
private Boolean supportsInplace;
@JsonProperty("loopable")
private Boolean loopable;
@JsonProperty("default_frame_length")
private Integer defaultFrameLength;
@JsonProperty("duration")
private Double duration;
@JsonProperty("gms_hash")
private GmsHash gmsHash;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("supports_inplace")
public Boolean getSupportsInplace() {
return supportsInplace;
}

@JsonProperty("supports_inplace")
public void setSupportsInplace(Boolean supportsInplace) {
this.supportsInplace = supportsInplace;
}

@JsonProperty("loopable")
public Boolean getLoopable() {
return loopable;
}

@JsonProperty("loopable")
public void setLoopable(Boolean loopable) {
this.loopable = loopable;
}

@JsonProperty("default_frame_length")
public Integer getDefaultFrameLength() {
return defaultFrameLength;
}

@JsonProperty("default_frame_length")
public void setDefaultFrameLength(Integer defaultFrameLength) {
this.defaultFrameLength = defaultFrameLength;
}

@JsonProperty("duration")
public Double getDuration() {
return duration;
}

@JsonProperty("duration")
public void setDuration(Double duration) {
this.duration = duration;
}

@JsonProperty("gms_hash")
public GmsHash getGmsHash() {
return gmsHash;
}

@JsonProperty("gms_hash")
public void setGmsHash(GmsHash gmsHash) {
this.gmsHash = gmsHash;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
