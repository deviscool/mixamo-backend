package cool.devis.mixamo.webservice;

import cool.devis.mixamo.util.entity.User;
import cool.devis.mixamo.util.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"cool.devis.mixamo"})
@EntityScan("cool.devis.mixamo.util.entity")
@EnableJpaRepositories("cool.devis.mixamo.util.repository")
public class Application {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        //read more https://spring.io/guides/gs/spring-boot/
        
    }
    
    @Bean
    public CommandLineRunner init(UserRepository userRepository){
        
        return (args)->{           
          
        };
    }
}
