/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.repository;

import cool.devis.mixamo.util.entity.CharacterAnimation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Eusebiu
 */
public interface CharacterAnimationRepository extends JpaRepository<CharacterAnimation, Integer> {
    
}
