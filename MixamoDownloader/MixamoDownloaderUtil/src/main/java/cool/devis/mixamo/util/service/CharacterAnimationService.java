/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cool.devis.mixamo.util.pojo.ExportDetails;
import cool.devis.mixamo.util.pojo.ExportPreferences;
import cool.devis.mixamo.util.pojo.ExportRequest;
import cool.devis.mixamo.util.pojo.PrimaryCharacter;
import cool.devis.mixamo.util.entity.CharacterAnimation;
import cool.devis.mixamo.util.entity.Product;
import cool.devis.mixamo.util.pojo.JobStatus;
import cool.devis.mixamo.util.repository.CharacterAnimationRepository;
import cool.devis.mixamo.util.pojo.Pagination;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Eusebiu
 */
@Service
public class CharacterAnimationService {

    @Autowired
    CharacterService characterService;

    @Autowired
    AnimationService animationService;

    @Autowired
    CharacterAnimationRepository characterAnimationRepository;

    @Value("${mixamo.api.key}")
    private String apiKey;

    public Page<CharacterAnimation> getCharacterAnimations(Pagination pagination) {

        PageRequest pageRequest = new PageRequest(pagination.getPage() - 1, pagination.getLimit());

        Page<CharacterAnimation> characterAnimations = characterAnimationRepository.findAll(pageRequest);

        return characterAnimations;
    }

    public ExportDetails getAnimationDetailsForExport(String characterId, String animationId, String authorizationJWT) {

        String url = "https://www.mixamo.com/api/v1/products/" + animationId + "?similar=0&character_id=" + characterId;

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.set("X-Api-Key", apiKey);
        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set("authorization", authorizationJWT);

        HttpEntity<String> entity = new HttpEntity<>("", headers);

        ResponseEntity<ExportDetails> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, ExportDetails.class);

        return exchange.getBody();
    }

    public JobStatus exportPrimaryCharacterAnimation(int animationId, String authorizationJWT) throws JsonProcessingException {
        PrimaryCharacter primaryCharacter = characterService.getPrimaryCharacter();

        Product animation = animationService.findById(animationId);

        JobStatus jobStatus = exportCharacterAnimation(animation, primaryCharacter.getPrimaryCharacterId(), authorizationJWT);

        Product character = characterService.findByUuid(primaryCharacter.getPrimaryCharacterId());

        if (character == null) {

            character = savePrimaryCharacter(primaryCharacter);
        }

        save(animation, character, jobStatus);

        return jobStatus;
    }

    private void save(Product animation, Product character, JobStatus jobStatus) {
        CharacterAnimation characterAnimation = new CharacterAnimation();

        characterAnimation.setAnimation(animation);
        characterAnimation.setCharacter(character);
        characterAnimation.setJobId(jobStatus.getStatus());
        characterAnimation.setJobStatus(jobStatus.getStatus());

        characterAnimationRepository.save(characterAnimation);
    }

    private Product savePrimaryCharacter(PrimaryCharacter primaryCharacter) {

        Product character = new Product();
        character.seUuId(primaryCharacter.getPrimaryCharacterId());
        character.setName(primaryCharacter.getPrimaryCharacterName());
        character.setSource(primaryCharacter.getPrimaryCharacterSource());
        character = characterService.save(character);
        return character;
    }

    public JobStatus exportCharacterAnimation(Product animation, String characterUuid, String authorizationJWT) throws JsonProcessingException {

        String url = "https://www.mixamo.com/api/v1/animations/export";

        String json = buildExportRequest(characterUuid, animation, authorizationJWT);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("X-Api-Key", apiKey);
        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set("authorization", authorizationJWT);

        HttpEntity<String> entity = new HttpEntity<>(json, headers);

        ResponseEntity<JobStatus> exchange = restTemplate.exchange(url, HttpMethod.POST, entity, JobStatus.class);

        return exchange.getBody();

    }

    private String buildExportRequest(String characterUuid, Product animation, String authorizationJWT) throws JsonProcessingException {
        ExportRequest request = new ExportRequest();
        ObjectMapper mapper = new ObjectMapper();

        ExportDetails exportDetails = this.getAnimationDetailsForExport(characterUuid, animation.getUuId(), authorizationJWT);

        List gmsHashList = new ArrayList();
        gmsHashList.add(exportDetails.getDetails().getGmsHash());

        request.setGmsHash(gmsHashList);
        request.setCharacterId(characterUuid);
        request.setProductName(animation.getName());

        request.setPreferences(getExportPreferences());

        String json = mapper.writeValueAsString(request);

        return json;
    }

    private ExportPreferences getExportPreferences() {
        ExportPreferences preferences = new ExportPreferences();
        preferences.setFormat("fbx7");
        preferences.setFps("30");
        preferences.setSkin("true");
        preferences.setReducekf("0");
        return preferences;
    }

    public JobStatus getJobStatus(String jobId, String authorizationJWT) {

        String url = "https://www.mixamo.com/api/v1/characters/" + jobId + "/monitor";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("X-Api-Key", apiKey);
        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set("authorization", authorizationJWT);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<JobStatus> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, JobStatus.class);

        return exchange.getBody();
    }

    public void downloadExportedFile(String url) throws MalformedURLException, IOException {

        File destination = new File("c://test//test.fbx");

        FileUtils.copyURLToFile(new URL(url), destination);
    }

}
