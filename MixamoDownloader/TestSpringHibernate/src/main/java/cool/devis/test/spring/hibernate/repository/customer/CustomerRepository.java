/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.test.spring.hibernate.repository.customer;

import cool.devis.test.spring.hibernate.entity.customer.Customer;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author eusebiu
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String test);
}