/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.webservice.controller;

import cool.devis.mixamo.util.entity.User;
import cool.devis.mixamo.util.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author eusebiu
 */
@RestController()
@RequestMapping(path = "user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(path = "register", method = RequestMethod.POST)
    public User register(@RequestBody @Valid User user) {

        return userService.saveUser(user);

    }

}
