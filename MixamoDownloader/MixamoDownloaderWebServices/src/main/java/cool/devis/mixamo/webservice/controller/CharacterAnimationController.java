/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.webservice.controller;

import cool.devis.mixamo.util.entity.CharacterAnimation;
import cool.devis.mixamo.util.pojo.JobStatus;
import cool.devis.mixamo.util.service.CharacterAnimationService;
import cool.devis.mixamo.util.pojo.Pagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author Eusebiu
 */
@RestController()
@RequestMapping(path = "/character/animation")
public class CharacterAnimationController {

    private static final Logger log = LoggerFactory.getLogger(CharacterAnimationController.class);

    String authorizationJWTMixamoSite = "";

    @Autowired
    CharacterAnimationService characterAnimationService;

    @RequestMapping(method = RequestMethod.GET)
    public Page<CharacterAnimation> getCharacterAnimations(@RequestParam(name = "page", defaultValue = "1", required = false) int page,
            @RequestParam(name = "limit", defaultValue = "20", required = false) int limit) {

        Pagination pagination = new Pagination();
        pagination.setPage(page);
        pagination.setLimit(limit);

        return characterAnimationService.getCharacterAnimations(pagination);
    }

    @RequestMapping(path = "/{animationId}", method = RequestMethod.POST)
    public JobStatus exportPrimaryCharacterAnimation(@PathVariable(name = "animationId") int animationId) throws Exception {

        JobStatus result = null;
        if (authorizationJWTMixamoSite.isEmpty()) {
            throw new Exception("authorizationJWTMixamoSite not set");
        }

        try {

            result = characterAnimationService.exportPrimaryCharacterAnimation(animationId, authorizationJWTMixamoSite);

        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw ex;
        }

        return result;
    }

    @RequestMapping(path = "/{characterId}/{animationId}", method = RequestMethod.POST)
    public String export(@PathVariable(name = "characterId") int characterId, @PathVariable(name = "animationId") int animationId) {

        throw new NotImplementedException();
    }
}
