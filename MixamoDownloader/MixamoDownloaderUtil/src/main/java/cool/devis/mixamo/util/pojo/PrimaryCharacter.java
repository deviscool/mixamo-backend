/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.pojo;

/**
 *
 * @author Eusebiu
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "primary_character_id",
    "primary_character_name",
    "primary_character_source"
})
public class PrimaryCharacter {

    @JsonProperty("primary_character_id")
    private String primaryCharacterId;
    @JsonProperty("primary_character_name")
    private String primaryCharacterName;
    @JsonProperty("primary_character_source")
    private String primaryCharacterSource;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("primary_character_id")
    public String getPrimaryCharacterId() {
        return primaryCharacterId;
    }

    @JsonProperty("primary_character_id")
    public void setPrimaryCharacterId(String primaryCharacterId) {
        this.primaryCharacterId = primaryCharacterId;
    }

    @JsonProperty("primary_character_name")
    public String getPrimaryCharacterName() {
        return primaryCharacterName;
    }

    @JsonProperty("primary_character_name")
    public void setPrimaryCharacterName(String primaryCharacterName) {
        this.primaryCharacterName = primaryCharacterName;
    }

    @JsonProperty("primary_character_source")
    public String getPrimaryCharacterSource() {
        return primaryCharacterSource;
    }

    @JsonProperty("primary_character_source")
    public void setPrimaryCharacterSource(String primaryCharacterSource) {
        this.primaryCharacterSource = primaryCharacterSource;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
