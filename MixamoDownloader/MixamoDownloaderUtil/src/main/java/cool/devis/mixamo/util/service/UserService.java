package cool.devis.mixamo.util.service;

import cool.devis.mixamo.util.repository.UserRepository;
import cool.devis.mixamo.util.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Eusebiu
 */
@Service("userService")
public class UserService {

    private UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bcryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User saveUser(User user) {

        user.setPassword(bcryptPasswordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findByConfirmationCode(String token) {
        return userRepository.findByConfirmationToken(token);
    }

}
