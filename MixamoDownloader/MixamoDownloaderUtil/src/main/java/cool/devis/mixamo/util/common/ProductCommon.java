/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.devis.mixamo.util.common;

import cool.devis.mixamo.util.entity.Product;
import cool.devis.mixamo.util.pojo.Pagination;
import cool.devis.mixamo.util.pojo.ProductsResult;
import org.springframework.data.domain.Page;

/**
 *
 * @author Eusebiu
 */
public class ProductCommon {

    public static ProductsResult getProductsResult(Page<Product> pageResult) {

        ProductsResult result = new ProductsResult();
        Pagination pagination = new Pagination();

        pagination.setNumPages(pageResult.getTotalPages());
        pagination.setNumResults((int) pageResult.getTotalElements());

        result.setPagination(pagination);
        result.setResults(pageResult.getContent());

        return result;
    }
}
